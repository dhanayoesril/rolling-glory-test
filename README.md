# README #

This is the Rolling Glory Front End Developer Test

### How to Run on Local ###
Make sure you have installed Node.js and npm. Clone respository and install the dependencies, then run the project :

```bash
git clone https://dhanayoesril@bitbucket.org/dhanayoesril/rolling-glory-test.git (run on terminal on your folder)
cd to the folder
npm install
npm start
```

### How to Deploy / Run on Static Server ###
For environments using Node, the easiest way to handle this would be to install serve and let it handle the rest:

```bash
npm install -g serve
serve -s build
```

The last command shown above will serve your static site on the port 3000. Like many of serve�s internal settings, the port can be adjusted using the -l or --listen flags:

```bash
serve -s build -l 4000
```

Run this command to get a full list of the options available:

```bash
serve -h
```

For more information about deployment, you can access this link => https://create-react-app.dev/docs/deployment/

### Deploy on Netlify ###
There is a website to deploy for free, one of which is Netlify. How to deploy on netlify :

```bash
- Go to https://www.netlify.com/ and sign up & login
- Add new sites
- Import an existing project
- Connect to Github or Gitlab or BitBucket
- Choose the repository
- On tab "Build Command", edit "npm run build" to "CI= npm run build"
- Deploy site
```

I have also deployed this project on netlify. You can access from this link => https://yusril-rolling-glory-test.netlify.app/

### Reason why using ReactJS ###
* Ease of Writing Components
* More Faster Rendering Process
* Reusable Component
* Easy to Learn
* Used by many Big Companies and Start-Ups
* Comes with a Toolset that makes it easy for Developers

### Best Practices###
* The are Functional Component (Hooks) and Class Component in React.js. In my experience, using Functional Component (Hooks) is more shorter, concise and efficient than using Class Component
* Reusable component make the coding more effective
* React.js is more easy to learn

### SEO Friendly Strategy ###
For SEO implementation, actually more optimal to use Next.js than React.js. But I never used Next.js before. So I just using React.js for finishing this test. If in work it is necessary to use Next js, then I will explore more about using Next.js. I'm a fast learner

### Other Information ###
* There are slightly different views, such as hover and image size
* Specification bonus that I have implemented are rating filter, stock filter, and responsive view